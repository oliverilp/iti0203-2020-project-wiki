# Retros
## Retro: Sprint 4 & Development Story
* ### What went well :smile:
    * All the main requirements for part 3 were doable without any major problems.
    * The entire team got to learn a lot of interesting new things.
    * Team is happy with the current outcome.

* ### What could have gone better :neutral_face:
    * Understanding spring security filters is fairly difficult.
    * PostqreSQL broke for seemingly no reason at one point, wasted a lot of time.

    
* ### Development Story:
    Part 1 was the hardest and most time-consuming. Learning Angular and Spring was the most difficult part in the whole project. Oliver was the one who ended up doing most of the front-end and Henri ended up doing most of the backend. Part 2 was the easiest and least time-consuming. Server setup went without any sort of problems. However, our IGDB api broke because it was suddenly upgraded to a new version and we had to slightly rework our code. Part 3 was more time consuming than part 2. Spring security was probably the most difficult part to understand on a conceptual level. PostqreSQL broke for seemingly no reason and a lot of time was wasted on that. 


## Retro: Sprint 3
* ### What went well :smile:
    * All the main requirements for part 2 were doable without any major problems.
    * The entire team got to learn a lot of interesting new things.
    * Certain aspects, like installing https, were much easier than expected.
    * Several extras done.
    * Team is happy with the current outcome.

* ### What could have gone better :neutral_face:
    * Route 53 charged me 1.20 >:( and I am not 100% sure if we need it.
    * Because of a lack of foresight on our side, we did not think about how to put our project into docker before we had already set up everything on the server. Adding docker after that would have been very bothersome.
    * Forgot about checking security/plaintext passwords in part 2

* ### What next :question:
    * Start working on part 3.
    * Divide the work.
    * Try and come up with a good basic logic for user management.
        
* ### What will we commit to improve in the next Sprint :question:
    * We will commit to improve our overall code.
    * Better communication is always a great goal to have.

* ### Compare retro 3 vs retro 1, 2. What is the progress :question:
    * All our sprints went relatively well. We did not encounter any extreme technological barriers, and we had enough time to implement our main priorities.
    * Progress is steady.
    * Core of the website is complete. Probably will not have to make any more changes to most currently existing controllers anymore.
    

## Retro: Sprint 2
* ### What went well :smile:
    * All main issues done
    * 3 backlog issues done
    * Part 1 ready for early delivery
    * Several extras done.
    * Found and removed several internal server errors.
    * Team is happy with the current outcome.

* ### What could have gone better :neutral_face:
    * Too many questions arise from the grading excel. Kinda hard to follow.
    * Did not have time to implement better database.
    * Misunderstood Q6. Could have gone way better.

* ### What next :question:
    * Start working on part 2.
    * Better db is a priority.
    * Start using aws.
        
* ### What will we commit to improve in the next Sprint :question:
    * We will commit to improve our overall code.
    * Better communication is always a great goal to have.

* ### Compare retro 2 vs retro 1. What is the progress :question:
    * Both our sprints went relatively well. We did not encounter any extreme technological barriers, and we had enough time to implement our main priorities.
    * There was no significant increase or decrease in efficiency between the retros. Progress is steady.
    
## Retro: Sprint 1
* ### What went well :smile:

    * Back and front connected
    * H2 runs well
    * IGDB api is working well
    * Basic website design and functionality is in place
    * Project is advancing at a steady pace
    * Learning Angular and Spring Boot was easier than expected 
    * Wiki and CI for it on GitLab pages works well

* ### What could have gone better :neutral_face:
    * Logic for holding reviews took a long time to get right
    * First experience with database relations was confusing
    * Angular Material is confusing
    * Creating a visually appealing and coherent design is difficult because you have to make most of it from scratch

* ### What next :question:
    * Three high priority issues
        * Add, read, delete(?) reviews
        * Filter games by their platforms or genres
        * Test coverage 20%
    * Do as much extra as possible
        * Angular material hopefully
        * Architecture drawing, might help us understand db relations better
        * Technical description
       
