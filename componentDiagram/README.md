# Component Diagram
      
## Diagram
<img src="./component_diagram.png" width="1708" style="padding-bottom: 100px; left: 50%;margin-left: -50vw;margin-right: -50vw;max-width: 1708px;position: relative;right: 50%;width: 1708px;">

## Description
Production is set up on AWS EC2 Ubuntu 20.04.1 LTS virtual machine.
Server was configured manually. 
For installation guide please read [the wiki](https://oliverilp.gitlab.io/iti0203-2020-project-wiki/serverInstallGuide/).
GitLab CI is used for the build and deploy process. 
It is made to run tests on all branches and deploy to main branch. 
Gitlab builds jar file for the backend and dist files for the frontend. 
Backend is setup as linux service/process. Backend configuration is in external .yaml file. 
Nginx is used to serve frontend files. Backend is connected to frontend on nginx using reverse proxy. 
Gitlab runner and production code are installed on the server. 
For production database PostgreSQL is used that is running in a docker with docker-compose. 
Tests are run in a separate h2 database.
Backend uses IGDB external API to retrieve info about games and saves it in our own database.
