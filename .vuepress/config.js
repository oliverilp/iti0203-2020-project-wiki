module.exports = {
    title: 'Critter',
    description: 'Documentation for ITI0203 project.',
    base: '/iti0203-2020-project-wiki/',
    dest: 'public',
    themeConfig: {
        nav: [
            {text: 'Home', link: '/'},
            {text: 'User Stories', link: '/stories/'},
            {text: 'Retros & Development Story', link: '/retros/'},
            {text: 'Usage Guide', link: '/usageGuide/'},
            {text: 'Server Install Guide', link: '/serverInstallGuide/'},
            {text: 'Component Diagram', link: '/componentDiagram/'}
        ]
    }
}
