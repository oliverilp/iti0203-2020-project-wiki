# User Stories

### As a < type of user >, I want < some goal > so that < some reason >. 

### 9 Must have business Issues (ALL DONE)
   * As a consumer, I want to filter and sort available game so that navigation is easier. **GUEST AND HIGHER**
   * As a consumer, I want to be able to rate games individually. **USER AND HIGHER**
   * As a consumer, I want to be able to see ratings from others. **GUEST AND HIGHER**
   * As a consumer, I want to get general info about a game. **USER AND HIGHER**
   * As an administrator, I want to be able to add games to the website. (Through the website) **ADMIN ONLY**
   * As a consumer, I want to have a mobile view. **GUEST AND HIGHER**
   * As an administrator, I want to be able to remove games from the website. (Through the website) **ADMIN ONLY**
   * As a consumer, I want to be able to look up games by name. **GUEST AND HIGHER**

   * As a consumer, I want to see a visually appealing website with color combinations that are easy on the eye.
### 8 Future Stories (NONE DONE)
   * As an administrator, I want to be able to delete reviews. To prevent spam etc. (Through the website). Complexity 1, Around 0.5-1 hours. Need to add button and link it to a request.
   * As a consumer, I want to be able to switch between light and dark mode. Complexity 3, Around 1-2 hours (may change, not sure if material and angular have an easy way to do this or not)
   * As a consumer, I want to have an overview of my reviews. Complexity 5, Around 1 - 1.5 
   * As a consumer, I want to be able to see X recent reviews. (not under game). Complexity 8, Around 1.5 - 2.5 hours. Mostly the same as previous but would also need to do some changes in the backend and db.
   * As a consumer, I want to be able to watch a trailer for a game under game details. Complexity 20, Around 5-6 hours, would need to rewrite certain IGDB api requests and then store the data. Then try displaying trailers in the front.
   * As a consumer, I want to be able to contact the admin and request games. Complexity 30, Around 12+ hours. Backend, db, frontend. 
   * As a consumer, I want to be able to reset my password with my email. Complexity 50. 1-2 days. Research and would definitely require learning a new technology.
   * As an entrepreneur, I want to make money off of my website. Add  adds to the site. Complexity 70. 2-3 days (maybe even longer). Would need to study some basic legality and explore our options before implementing.

