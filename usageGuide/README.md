# Usage Guide

## What can you do?
   * ### Add games to critter
        With the "add game" bar you can add games to the site by simply inserting the name.   
        Because the games are takes from IGDB. The admin is expected use the correct name of the game for this process.   
        Please use this [link](https://www.igdb.com/discover) to get the correct name.
   * ### Search games by name
        Game list is updated in real time with every single input letter.
   * ### Filter games by genre and/or platform
        Filter can be selected from dropdown lists and will be applied if you press "Add filter".
   * ### See details about game
        By clicking on a game, you will be redirected to the game details page.
   * ### Write reviews
        Reviews can be written under game details.
   * ### See reviews
        Reviews can be read under game details.
        Game ratings will be updated according to reviews.
        
## What do we use?
   * ### Spring Boot, Gradle and Java for the back-end.
   * ### H2 for database.
   * ### Angular.js, Angular Material for the front-end.
   * ### IGDB api as our external [api](https://api-docs.igdb.com/).
      GameController post requests make a request to IGDB and after parsing the data adds the entity to our database.
      