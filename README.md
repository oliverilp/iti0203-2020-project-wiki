---
home: true
heroText: Critter
tagline: Documentation for ITI0203 project
actionText: User Stories →
actionLink: /stories/
features:
- title: Business Analysis
  details: We plan on creating a "simple modern" website for reviewing games. By gathering review scores from average players, we plan to provide a better overview of how certain games are received by the greater masses.  You might think "this sounds a lot like Metacritic", but you are mistaken because while Metacritic has user scores, it only highlights critic scores. In the game industry, critic scores are known to be biased and unreliable and therefore go against our websites principles. Critter uses the IGDB api to have a semi automated way of adding games to the site. Design inspired by HowLongToBeat.com.
footer: Oliver Ilp, Henri Helisma
---
