# Part 0: Nice to know

Get into server if SSH is set up
````bash
    ssh ubuntu@13.53.135.207
````
Location of Nginx configuration file
````bash
    cd /etc/nginx/sites-available
    nano java-critter ## sudo to edit
````
Location of cert files for HTTPS (generated with certbot)
````bash
    cd /etc/letsencrypt$
````

# Part 1: Getting Started

## Connecting to Server

Get pem file (ask Henri)  
Put the file in your `user/ssh` folder  
`ssh -i .ssh/critter.pem ubuntu@13.53.135.207`

To make your life easier
```bash  
    cd .ssh/   
    sudo nano authorized_keys
```
add your public key to the file (you can open your key using a text editor, generating one is system based, google it)  
ctrl + X to exit, enter to confirm (wont repeat this part later)

## More memory, less problems. Add memory for server.

Run `htop` to check your current swap memory. For this server 2GB is needed, if it is 2 and does not reset with server reboot then skip this part.
````bash
    sudo fallocate -l 2G /swapfile  
    sudo chmod 600 /swapfile  
    sudo mkswap /swapfile  
    sudo swapon /swapfile  
    echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab ### This is necessary to keep the config after reboot.
````
[More in-depth info](https://itsfoss.com/create-swap-file-linux/)

## Install updates and main requirements
````bash
    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get install openjdk-11-jre openjdk-11-jdk
````

# Part 2: Install gitlab runner

## Server 
Go into backend GitLab repository. Setting -> CI/CD Settings -> Runners. Get runner registration token.
````bash
    curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
    dpkg -i gitlab-runner_amd64.deb # might need sudo
    
    sudo gitlab-runner register ## Set URL to https://gitlab.cs.ttu.ee/. Set token to the one that you found earlier. Skip description. Set tag to "critter".     

    sudo su gitlab-runner ## To get into gitlab-runner user
    nano /home/gitlab-runner/.bash_logout
````
In file comment out all 3 lines. Save.

````bash  
    exit ## To get back into ubuntu user
    sudo service gitlab-runner start
    sudo service gitlab-runner status ## To check if everything is working.
````
[More info here](https://docs.gitlab.com/runner/install/linux-manually.html) 

## Backend 
Check if `.gitlab-ci.yml` looks like this. If does not exist, make it under `src` folder.
````yaml
    stages:
      - build
      - test
      - deploy
    
    before_script:
      - export GRADLE_USER_HOME=`pwd`/.gradle
    
    build critter:
      stage: build
      cache:
        paths:
          - .gradle/wrapper
          - .gradle/caches
      artifacts:
        paths:
          - build/libs
      tags:
        - critter
      script:
        - ./gradlew assemble
    
    test critter:
      stage: test
      tags:
        - critter
      script:
        - ./gradlew check
    
    deploy critter:
      stage: deploy
      only:
        refs:
          - main
      tags:
        - critter
      script:
        - mkdir -p ~/api-deployment # mkdir make folder api-deployment ~/ is under current user directory so for gitlab it would be /home/gitlab/api-deployment
        - rm -rf ~/api-deployment/* # rm remove -rf is recursive files from api-deployment
        - cp -r build/libs/. ~/api-deployment # cp - copy build/libs is where #- sudo service critter restart  # this requires sudo rights for gitlab user
        - docker-compose -f ~/docker-compose.yml down # Needs to be in the end product put not until docker is installed.
        - docker-compose -f ~/docker-compose.yml up -d # Needs to be in the end product put not until docker is installed.
        - sudo service java-critter restart # Needs to be in the end product but not until service has been created.
````
Check if `build.gradle` has the following line, if not then add. This sets jar name.
````gradle
    bootJar {
        archiveFileName = 'java-critter.jar'
    }
````

## Confirming
If the previous steps have been done correctly then check if jar works in the server. (After comitting&pushing and passing pipeline).
````bash  
    sudo su gitkab-runner
    cd /api-deployment
    java -jar java-critter.jar
````
If jar runs then `13.53.135.207:8080/api/swagger-ui/` should show project swagger.

# Part 3: Create Service
````bash
    cd /etc/systemd/system/ 
    sudo touch java-critter.service
    sudo nano java-critter.service
````
Set the content to the following 
````bash
    [Unit]
    Description=dashboard heroes service
    After=network.target
    
    [Service]
    Type=simple
    User=gitlab-runner
    WorkingDirectory=/home/gitlab-runner/api-deployment
    ExecStart=/usr/bin/java -jar java-critter.jar
    Restart=on-abort
    
    [Install]
    WantedBy=multi-user.target
````
Sava, exit and then.

````bash
    sudo systemctl daemon-reload
    systemctl enable java-critter
    sudo service java-critter restart
    
    sudo visudo
````
Add this to the file. This will disable sudo password for gitlab-runner.
````bash
    gitlab-runner ALL = NOPASSWD: /usr/sbin/service java-critter *
````

Now you can enable reboot in yaml file.

# Part 4: Create gitlab-runner for frontend
Check if `.gitlab-ci.yml` looks like this. If does not exist, make it under `src` folder.
````yaml
    stages:
      - build
      - deploy
    
    build critter:
      stage: build
      image: node:12-alpine
      cache:
        paths:
          - node_modules
      artifacts:
        paths:
          - dist
      tags:
        - critter-front
      script:
        - npm install
        - npm run build
    
    deploy critter:
      stage: deploy
      tags:
        - critter-front
      script:
        - mkdir -p ~/front-deployment
        - rm -rf ~/front-deployment/*
        - cp -r dist/. ~/front-deployment
        - sudo service java-critter restart # Needs to be in the end product but not until service has been created.
````
Go into frontend gitlab repository. Setting -> CI/CD Settings -> Runners. Get runner registration token.
````bash  
    sudo gitlab-runner register ## Set URL to https://gitlab.cs.ttu.ee/. Set token to the one that you found earlier. Skip description. Set tag to "critter-front". 
````

#Part 5: Install Node and Nginx
## Install
````bash
    sudo curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
    sudo apt-get install -y nodejs
    sudo apt-get install nginx
````

## Set up Nginx
On a clean install only default files will exist.
````bash  
    cd /etc/nginx/sites-available
    sudo cp default java-critter
    cd ..
    cd sites-enabled
    sudo ln -s /etc/nginx/sites-available/java-critter /etc/nginx/sites-enabled/
    sudo rm default
    cd ..
    cd sites-available
    sudo nano java-critter
````
Replace first `server {}` content with the following.
````bash
server {
        listen 80 default_server;
        listen [::]:80 default_server;

        # SSL configuration
        #
        # listen 443 ssl default_server;
        # listen [::]:443 ssl default_server;
        #
        # Note: You should disable gzip for SSL traffic.
        # See: https://bugs.debian.org/773332
        #
        # Read up on ssl_ciphers to ensure a secure configuration.
        # See: https://bugs.debian.org/765782
        #
        # Self signed certs generated by the ssl-cert package
        # Don't use them in a production server!
        #
        # include snippets/snakeoil.conf;

        root /var/www/front-deployment/critter-frontend;

        # Add index.php to the list if you are using PHP
        index index.html index.htm index.nginx-debian.html;

        server_name _;

        location /api/ {
                 proxy_pass   http://localhost:8080;
        }
        location / {
        index  index.html index.htm;
        if (!-e $request_filename){
          rewrite ^(.*)$ /index.html break;
        }
    }
}

````

`13.53.135.207` should now display frontend

#Part 6: Install https
````bash  
    sudo snap install --classic certbot
    sudo certbot --nginx
````
If everything has gone correctly then HTTPS should be installed on the server, and the Nginx conf file should look like this. (Excluding comments)
````bash  
    server {
            listen 80 default_server;
            listen [::]:80 default_server;
    
            root /var/www/front-deployment/critter-frontend;
    
            index index.html index.htm index.nginx-debian.html;
    
            server_name _;
    
            location /api/ {
                     proxy_pass   http://localhost:8080;
            }
            location / {
            index  index.html index.htm;
            if (!-e $request_filename){
              rewrite ^(.*)$ /index.html break;
            }
        }
    }
    server {
    
            root /var/www/front-deployment/critter-frontend;
    
            index index.html index.htm index.nginx-debian.html;
        server_name critter.oliverilp.ee; # managed by Certbot
    
    
            location /api/ {
                     proxy_pass   http://localhost:8080;
            }
            location / {
            index  index.html index.htm;
            if (!-e $request_filename){
              rewrite ^(.*)$ /index.html break;
            }
        }
    
    
        listen [::]:443 ssl ipv6only=on; # managed by Certbot
        listen 443 ssl; # managed by Certbot
        ssl_certificate /etc/letsencrypt/live/critter.oliverilp.ee/fullchain.pem; # managed by Certbot
        ssl_certificate_key /etc/letsencrypt/live/critter.oliverilp.ee/privkey.pem; # managed by Certbot
        include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
    
    }
    server {
        if ($host = critter.oliverilp.ee) {
            return 301 https://$host$request_uri;
        } # managed by Certbot
    
    
            listen 80 ;
            listen [::]:80 ;
        server_name critter.oliverilp.ee;
        return 404; # managed by Certbot
    
    
    }
````

# Part 7: Install Docker for PostgreSQL

Install Docker packages.

````bash
    sudo apt-get install docker
    sudo apt-get install docker-compose
````

Copy `docker-compose.yml` file from [backend repo](https://gitlab.cs.ttu.ee/heheli/iti0203-2020-project-backend) to `/home/gitlab-runner/` directory.

````bash
    scp docker-compose.yml ubuntu@13.53.135.207:/home/gitlab-runner/
````

Now next time when GitLab Runner starts, it will automatically download the PostgreSQL image and start the container. 
